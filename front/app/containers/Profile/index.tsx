import * as React from 'react';
// import * as style from './style.css';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/reducers';

namespace Profile {
	export interface Props extends RouteComponentProps<any, any> {
	}
}

@connect(
	(state: RootState): Pick<Profile.Props, any> => {
		return {};
	},
	(dispatch: Dispatch): Pick<Profile.Props, any> => ({})
)

class Profile extends React.Component<Profile.Props> {
	static defaultProps: Partial<Profile.Props> = {};

	constructor(props: Profile.Props, context?: any) {
		super(props, context);
	}

	render() {

		return (
			<div>
				Profile
			</div>
		);
	}
}

export default Profile;