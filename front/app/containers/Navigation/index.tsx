import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps, browserHistory } from 'react-router';
import { RootState } from 'app/reducers';
// styles
import './style.scss';
// icons
const profileIcon = require('app/../assets/images/icon_profile.svg') as string;
const partnersIcon = require('app/../assets/images/icon_partners.svg') as string;
const usersIcon = require('app/../assets/images/icon_users.svg') as string;
const dmpIcon = require('app/../assets/images/icon_DMP.svg') as string;
const menuCollapseIcon = require('app/../assets/images/burger_menu_collapse.svg') as string;
const menuExpandIcon = require('app/../assets/images/burger_menu_expand.svg') as string;

namespace Navigation {
	export interface Props extends RouteComponentProps<any, any> {
	}

	export interface State {
		toggleMenu: boolean
	}
}

@connect(
	(state: RootState): Pick<Navigation.Props, any> => {
		return {};
	},
	(dispatch: Dispatch): Pick<Navigation.Props, any> => ({})
)

class Navigation extends React.Component<Navigation.Props, Navigation.State> {
	static defaultProps: Partial<Navigation.Props> = {};

	state = {
		toggleMenu: false
	};

	constructor(props: Navigation.Props, context?: any) {
		super(props, context);
	}

	onToggleMenu(value: boolean) {
		if (this.state.toggleMenu === value) {
			return;
		}

		this.setState({ toggleMenu: value });
	}

	goTo(url: string) {
		browserHistory.push(url)
	}

	render() {
		const { children } = this.props;

		const minimized = this.state.toggleMenu;

		return (
			<div className="row" style={{ height: '100%' }}>
				<div className="left-bar-menu col-lg-2"
						 style={{
							 backgroundColor: '#3e6fe4', maxWidth: minimized ? 50 : undefined,
							 display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
						 }}
				>
					<div>
						<img src={this.state.toggleMenu ? menuExpandIcon : menuCollapseIcon} alt=""
								 style={{ height: 35, maxWidth: 50 }}
								 onClick={this.onToggleMenu.bind(this, !minimized)}/>
						<div style={{ padding: '40px 0' }}>
							<div style={({
								marginLeft: '20%', color: '#fff66c', fontSize: '24px',
								fontWeight: '600',
								display: minimized ? 'none' : 'block',
								lineHeight: 1,
								position: 'relative',
								width: 120
							} as any)}>
								<div>Простой</div>
								<div>Mир</div>
								<img src={usersIcon} alt=""
										 style={{
											 height: 40,
											 position: 'absolute',
											 right: -20,
											 top: -25
										 }}/>
							</div>
						</div>
						<div className="menu-item row active" style={{ minHeight: 56, display: 'flex' }}
								 {...{ onTouchTap: this.goTo.bind(this, '/profile') }}>
							<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>
								<img src={profileIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{
										 flex: 10,
										 paddingLeft: 10,
										 alignItems: 'center',
										 display: minimized ? 'none' : 'flex'
									 }}>
								Профиль
							</div>
						</div>
						<div className="menu-item row" style={{ minHeight: 56, display: 'flex' }}
								 {...{ onTouchTap: this.goTo.bind(this, '/partners') }}>
							<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>
								<img src={partnersIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{
										 flex: 10,
										 paddingLeft: 10,
										 alignItems: 'center',
										 display: minimized ? 'none' : 'flex'
									 }}>
								Партнеры
							</div>
						</div>
						<div className="menu-item row" style={{ minHeight: 56, display: 'flex' }}
								 {...{ onTouchTap: this.goTo.bind(this, '/users') }}>
							<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>
								<img src={usersIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{
										 flex: 10,
										 paddingLeft: 10,
										 alignItems: 'center',
										 display: minimized ? 'none' : 'flex'
									 }}>
								Пользователи
							</div>
						</div>
						<div className="menu-item row" style={{ minHeight: 56, display: 'flex' }}>
							<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>
								<img src={dmpIcon} alt=""/>
							</div>
							<div className={`col-xs-10`}
									 style={{
										 flex: 10,
										 paddingLeft: 10,
										 alignItems: 'center',
										 display: minimized ? 'none' : 'flex'
									 }}>
								DMP данные
							</div>
						</div>
					</div>

					<div style={{ marginBottom: 20, display: 'flex', justifyContent: 'center' }}>
						<button className="btn btn-primary"
										style={{
											borderRadius: '20px',
											width: '60%',
											justifyContent: 'center',
											display: minimized ? 'none' : 'flex'
										}}
						>
							Выход
						</button>
					</div>

					<div className="menu-item row" style={{
						marginBottom: 20, minHeight: 56,
						display: minimized ? 'flex' : 'none'
					}}>
						<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>
							<img src={dmpIcon} alt=""/>
						</div>
						<div className={`col-xs-10`}
								 style={{
									 flex: 10,
									 paddingLeft: 10,
									 alignItems: 'center',
									 display: minimized ? 'none' : 'flex'
								 }}>
							Выход
						</div>
					</div>


				</div>
				<div className="col-lg-10">
					{children}
				</div>
			</div>
		);
	}
}

export default Navigation;