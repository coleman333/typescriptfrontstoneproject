import * as React from 'react';
// import * as style from './style.css';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/reducers';

namespace Users {
	export interface Props extends RouteComponentProps<any, any> {
	}
}

@connect(
	(state: RootState): Pick<Users.Props, any> => {
		return {};
	},
	(dispatch: Dispatch): Pick<Users.Props, any> => ({})
)

class Users extends React.Component<Users.Props> {
	static defaultProps: Partial<Users.Props> = {};

	constructor(props: Users.Props, context?: any) {
		super(props, context);
	}

	render() {

		return (
			<div >
				Users
			</div>
		);
	}
}

export default Users;